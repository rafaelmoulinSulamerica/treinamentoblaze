//
// Blaze Advisor Server Deployment.
// Created by the Blaze Advisor Generate Deployment Wizard
//

// This material is the confidential, unpublished property of 
// Fair Isaac Corporation. Receipt or possession of this material does not 
// convey rights to divulge, reproduce, use, or allow others to use it 
// without the specific written authorization of Fair Isaac Corporation and 
// use must conform strictly to the license agreement.
//
// Copyright (c) 2000-2017 Fair Isaac Corporation. All Rights Reserved.
// 
//

package qdtarsetkm2018;

import com.blazesoft.server.base.NdService;
import com.blazesoft.server.base.NdServiceSessionException;
import com.blazesoft.server.base.NdServiceException;
import com.blazesoft.server.base.NdServerException;
import com.blazesoft.server.deploy.NdStatelessServer;
import com.blazesoft.server.config.NdServerConfig;
import com.blazesoft.server.local.NdLocalServerException;

import java.io.*;
import java.util.*;

//==> Import Application specific code here

/**
 *	This class implements a stateless server based on TarSetKm2018
 *	It is a subclass of <code>com.blazesoft.server.deploy.NdStatelessServer</code>.
 */
public class Server extends NdStatelessServer

{
	/**
	 *	Constructor. Calls the constructor of the superclass.
	 *	@param	serverConfig	Server configuration to use
	 *
 	 *	@exception NdLocalServerException if construction of the server fails.
	 */
	public Server(NdServerConfig serverConfig)
			throws NdLocalServerException
	{
		super(serverConfig);
	}

	/**
	 *	Invokes a server through the entry point "calcularQuilometragem"
	 *	in the service "TarSetKm2018".
	 *
	 *	@param	arg0		==> Enter a description here@param	arg1		==> Enter a description here@param	arg2		==> Enter a description here
	 *	@return	br.com.sulamerica.auto.negocio.calculo.Calculo	==> Enter a description of the return value
	 */
	public br.com.sulamerica.auto.negocio.calculo.Calculo calcularQuilometragem(br.com.sulamerica.auto.negocio.calculo.Calculo arg0, br.com.sulamerica.auto.alcada.Alcada arg1, String arg2)
			throws NdServerException, NdServiceException, NdServiceSessionException
	{
		// Build the argument list
		Object[] applicationArgs = new Object[3];
		applicationArgs[0] = arg0;
			applicationArgs[1] = arg1;
			applicationArgs[2] = arg2;
			

		// Invoke the service and returns its result, if any.
		br.com.sulamerica.auto.negocio.calculo.Calculo retVal = (br.com.sulamerica.auto.negocio.calculo.Calculo)invokeService("TarSetKm2018", "calcularQuilometragem", null, applicationArgs);
		return retVal;
	}
	/**
	 *	Invokes a server through the entry point "calcularEndossoQuilometragem"
	 *	in the service "TarSetKm2018".
	 *
	 *	@param	arg0		==> Enter a description here@param	arg1		==> Enter a description here@param	arg2		==> Enter a description here
	 *	@return	br.com.sulamerica.auto.negocio.calculo.Calculo	==> Enter a description of the return value
	 */
	public br.com.sulamerica.auto.negocio.calculo.Calculo calcularEndossoQuilometragem(br.com.sulamerica.auto.negocio.calculo.Calculo arg0, br.com.sulamerica.auto.alcada.Alcada arg1, String arg2)
			throws NdServerException, NdServiceException, NdServiceSessionException
	{
		// Build the argument list
		Object[] applicationArgs = new Object[3];
		applicationArgs[0] = arg0;
			applicationArgs[1] = arg1;
			applicationArgs[2] = arg2;
			

		// Invoke the service and returns its result, if any.
		br.com.sulamerica.auto.negocio.calculo.Calculo retVal = (br.com.sulamerica.auto.negocio.calculo.Calculo)invokeService("TarSetKm2018", "calcularEndossoQuilometragem", null, applicationArgs);
		return retVal;
	}

}
