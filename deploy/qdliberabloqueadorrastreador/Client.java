//
// Blaze Advisor Server Deployment.
// Created by the Blaze Advisor Generate Deployment Wizard
//

// This material is the confidential, unpublished property of 
// Fair Isaac Corporation. Receipt or possession of this material does not 
// convey rights to divulge, reproduce, use, or allow others to use it 
// without the specific written authorization of Fair Isaac Corporation and 
// use must conform strictly to the license agreement.
//
// Copyright (c) 2000-2017 Fair Isaac Corporation. All Rights Reserved.
// 
//

package qdliberabloqueadorrastreador;

import com.blazesoft.server.base.NdServiceSessionException;
import com.blazesoft.server.base.NdServiceException;
import com.blazesoft.server.base.NdServerException;
import com.blazesoft.server.base.NdServiceSessionResultEvent;
import com.blazesoft.util.NdWrappedRuntimeException;


import java.io.*;
import java.util.*;

//==> Import Application specific code here

/**
 *	This class implements a client of the Server.
 *	Note that this is simply a client example. Its intent is to show how,
 *	once a reference to a server is obtained, a client may invoke the
 *	server's entry points.
 *	<p>
 *	<pre>
 *	The methods of this class are organized as follows:
 *		"business invocation methods"
 *			-> There is one such method per server invocation entry-point. These
 *			   methods directly invoke the server's entry points with the
 *			   arguments they are passed.
 *		"business invocation method triggers"
 *			-> There is one such method per server invocation entry-point. These
 *			   methods should be modified by the user in order to specify the
 *			   arguments to be passed to them.
 *		"run method"
 *			-> This method simply invokes the business invocation triggers one
 *			   by one.
 *	</pre>
 */
public class Client implements Runnable
{
	// Reference to the  server.
	private Server _server;

	/**
	 *	Constructor for a client
	 *
	 *	@param	server	Server to connect to
	 */
	public Client(Server server)
	{
		// Keep track of the server. This is just an example: real
		// applications obtain references to the server just prior to
		// stateless invocations or the creation of sessions.
		_server = server;
	}

	/**
	 *	Invokes the Server server through the
	 *	"LiberarRastreador" entry point.
	 *
	 *	@param	arg0		==> Enter a description here	 *	@param	arg1		==> Enter a description here
	 *	@return	br.com.sulamerica.auto.negocio.calculo.Calculo	==> Enter a description of the return value
	 */
	public br.com.sulamerica.auto.negocio.calculo.Calculo liberarRastreador(br.com.sulamerica.auto.negocio.calculo.Calculo arg0, br.com.sulamerica.auto.alcada.Alcada arg1)
			throws NdServerException, NdServiceException, NdServiceSessionException
	{
		// Invoke the service's entry point.
		br.com.sulamerica.auto.negocio.calculo.Calculo retVal = (br.com.sulamerica.auto.negocio.calculo.Calculo)_server.liberarRastreador(arg0, arg1);
		return retVal;
	}

	/**
	 *	Triggers the invocation of the Server server through the
	 *	"LiberarRastreador" entry point.
	 *	This method contains commented out code that should be enabled in order to
	 *	trigger the invocation of the business method with meaningful arguments.
	 */
	private br.com.sulamerica.auto.negocio.calculo.Calculo _liberarRastreador()
			throws NdServerException, NdServiceException, NdServiceSessionException
	{
		//==> Set up the invocation arguments.
		//==>	br.com.sulamerica.auto.negocio.calculo.Calculo arg0 = (value for arg0);
		//==>	br.com.sulamerica.auto.alcada.Alcada arg1 = (value for arg1);
		//==> Invoke the actual method. Replace the next line with:
		//==>	return liberarRastreador(arg0, arg1);
		return null;
	}
	/**
	 *	Implements the Runnable interface.
	 *	This implementation simply invokes the business invocation methods
	 */
	public void run()
	{
		try {
			// Invoke each entry point
			_liberarRastreador();
		}
		catch (Exception e) {
			// Wrap the exception and throw it
			throw new NdWrappedRuntimeException(e);
		}
	}
}
