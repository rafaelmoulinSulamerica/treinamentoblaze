//
// Blaze Advisor Server Deployment.
// Created by the Blaze Advisor Generate Deployment Wizard
//

// This material is the confidential, unpublished property of 
// Fair Isaac Corporation. Receipt or possession of this material does not 
// convey rights to divulge, reproduce, use, or allow others to use it 
// without the specific written authorization of Fair Isaac Corporation and 
// use must conform strictly to the license agreement.
//
// Copyright (c) 2000-2017 Fair Isaac Corporation. All Rights Reserved.
// 
//

package qdfrota;

import com.blazesoft.server.base.NdService;
import com.blazesoft.server.base.NdServiceSessionException;
import com.blazesoft.server.base.NdServiceException;
import com.blazesoft.server.base.NdServerException;
import com.blazesoft.server.deploy.NdStatelessServer;
import com.blazesoft.server.config.NdServerConfig;
import com.blazesoft.server.local.NdLocalServerException;

import java.io.*;
import java.util.*;

//==> Import Application specific code here

/**
 *	This class implements a stateless server based on Frota
 *	It is a subclass of <code>com.blazesoft.server.deploy.NdStatelessServer</code>.
 */
public class Server extends NdStatelessServer

{
	/**
	 *	Constructor. Calls the constructor of the superclass.
	 *	@param	serverConfig	Server configuration to use
	 *
 	 *	@exception NdLocalServerException if construction of the server fails.
	 */
	public Server(NdServerConfig serverConfig)
			throws NdLocalServerException
	{
		super(serverConfig);
	}

	/**
	 *	Invokes a server through the entry point "validarCadastroFrota"
	 *	in the service "Frota".
	 *
	 *	@param	arg0		==> Enter a description here@param	arg1		==> Enter a description here
	 *	@return	br.com.sulamerica.auto.negocio.frota.Frota	==> Enter a description of the return value
	 */
	public br.com.sulamerica.auto.negocio.frota.Frota validarCadastroFrota(br.com.sulamerica.auto.negocio.frota.Frota arg0, br.com.sulamerica.auto.alcada.Alcada arg1)
			throws NdServerException, NdServiceException, NdServiceSessionException
	{
		// Build the argument list
		Object[] applicationArgs = new Object[2];
		applicationArgs[0] = arg0;
			applicationArgs[1] = arg1;
			

		// Invoke the service and returns its result, if any.
		br.com.sulamerica.auto.negocio.frota.Frota retVal = (br.com.sulamerica.auto.negocio.frota.Frota)invokeService("Frota", "validarCadastroFrota", null, applicationArgs);
		return retVal;
	}
	/**
	 *	Invokes a server through the entry point "liberarLoteFrota"
	 *	in the service "Frota".
	 *
	 *	@param	arg0		==> Enter a description here
	 *	@return	br.com.sulamerica.auto.negocio.frota.Frota	==> Enter a description of the return value
	 */
	public br.com.sulamerica.auto.negocio.frota.Frota liberarLoteFrota(br.com.sulamerica.auto.negocio.frota.Frota arg0)
			throws NdServerException, NdServiceException, NdServiceSessionException
	{
		// Build the argument list
		Object[] applicationArgs = new Object[1];
		applicationArgs[0] = arg0;
			

		// Invoke the service and returns its result, if any.
		br.com.sulamerica.auto.negocio.frota.Frota retVal = (br.com.sulamerica.auto.negocio.frota.Frota)invokeService("Frota", "liberarLoteFrota", null, applicationArgs);
		return retVal;
	}
	/**
	 *	Invokes a server through the entry point "validarLoteFrota"
	 *	in the service "Frota".
	 *
	 *	@param	arg0		==> Enter a description here
	 *	@return	br.com.sulamerica.auto.negocio.frota.Frota	==> Enter a description of the return value
	 */
	public br.com.sulamerica.auto.negocio.frota.Frota validarLoteFrota(br.com.sulamerica.auto.negocio.frota.Frota arg0)
			throws NdServerException, NdServiceException, NdServiceSessionException
	{
		// Build the argument list
		Object[] applicationArgs = new Object[1];
		applicationArgs[0] = arg0;
			

		// Invoke the service and returns its result, if any.
		br.com.sulamerica.auto.negocio.frota.Frota retVal = (br.com.sulamerica.auto.negocio.frota.Frota)invokeService("Frota", "validarLoteFrota", null, applicationArgs);
		return retVal;
	}

}
